module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  rules: {
    'import/extensions': ['error', 'always', {
      js: 'never',
      vue: 'never',
    }],
    'import/no-named-as-default': ['off'],
    camelcase: [0, { properties: 'always' }],
    // 'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // 'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'max-len': 0,
    'no-param-reassign': 0,
    'no-unused-vars': 0,
    'no-shadow': ['error', { builtinGlobals: false, hoist: 'never', allow: ['state', 'getters'] }],
    'prefer-destructuring': ['error', { object: false, array: false }],
    'object-curly-newline': ['off', { multiline: false }],
  },
  parserOptions: {
    parser: 'babel-eslint',
  },

};
