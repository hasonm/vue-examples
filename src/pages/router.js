// import Vue from 'vue';
import Router from 'vue-router';
// import Home from './Home.vue';
import Home from './Home';

// Vue.use(Router);

export const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        layout: 'nav-default',
      },
    },
    {
      path: '/essentials',
      name: 'Essentials',
      component: () => import(/* webpackChunkName: "about" */ './Essentials'),
      meta: {
        layout: 'nav-left',
      },
      query: {
        'background-color': 'orange',
      },
    },
    {
      path: '/forms',
      name: 'Forms',
      component: () => import(/* webpackChunkName: "about" */ './Forms'),
      meta: {
        layout: 'nav-left',
      },
      query: {},
    },
    {
      path: '/profile/:profileID',
      name: 'Profile',
      component: () => import(/* webpackChunkName: "about" */ './Profile'),
      meta: {
        layout: 'nav-right',
      },
      query: {},
    },
    {
      path: '*',
      name: '404',
      layout: 'notFound',
      // component: () => import(/* webpackChunkName: "about" */ './Essentials'),
    },
  ],
});

export default router;
