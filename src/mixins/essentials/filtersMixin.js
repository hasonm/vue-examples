
const filtersMixin = {
  filters: {
    fm_upprecase(string) {
      return string.toUpperCase();
    },
    fm_lowercase(string) {
      return string.toLowerCase();
    },
  },
};

export default filtersMixin;
