
const methodsMixin = {
  methods: {
    mm_getCharCodes(string) {
      return (string.split('')).map(ch => ch.charCodeAt(0));
    },
  },
};

export default methodsMixin;
