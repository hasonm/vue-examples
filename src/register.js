// https://vuejs.org/v2/guide/components-registration.html#Automatic-Global-Registration-of-Base-Components
import Vue from 'vue';
import upperFirst from 'lodash/upperFirst';
import camelCase from 'lodash/camelCase';

const requireComponent = require.context(
  // The relative path of the components folder
  './components/globals',
  // Whether or not to look in subfolders
  true,
  // The regular expression used to match base component filenames
  /(.*?)\w+\.(vue)$/,
  // /[A-Z]\w+\.(vue|js)$/,


);

requireComponent.keys().forEach((fileName) => {
  const componentConfig = requireComponent(fileName);
  const componentName = upperFirst(camelCase(fileName.replace(/^\.\/(.*)\.\w+$/, '$1')));
  Vue.component(
    componentName,
    componentConfig.default || componentConfig,
  );
});
