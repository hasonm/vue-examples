const state = {
    profiles: {
        1: { id: 1, name: 'Jane Doe' },
    },
};

// state,commit,dispatch, getters,root_getters
const getters = {
    get_profile: (state, getters) => id => (state.profiles[id]),
    get_jane_doe: state => state.profiles[1],
    get_state: state => state,

};
const actions = {
    set_profile({ commit, getters, state }, { id, data }) {
        commit('set_profile', { id, data });
    },
};

const mutations = {
    set_profile(state, { id, data }) {
        if (state.profiles[id]) {
            state.profiles[id].data = { ...state.profiles[id].data, ...data };
            return;
        }
        state.profiles[id] = { id, data };
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};
