import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import profiles from './modules/profiles';
// ...


Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    profiles,
  },
  // strict: DEBUG,
  plugins: [createPersistedState({
    // paths: [],
    // filter: mutation =>
    // mutation.type !== 'notification/set',
  })],
});

export default store;
