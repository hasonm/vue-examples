import Vue from 'vue';
import Router from 'vue-router';
import VeeValidate from 'vee-validate';
import App from '@/App';
import PortalVue from 'portal-vue';
import router from '@/pages/router';
import store from '@/store/store';
import '@/styles/main.scss';
// import './components/globals/register';
import './register';
import './custom-directives/c_directives';

Vue.config.productionTip = false;
Vue.config.devtools = true;
Vue.config.performance = true;
// Vue.use(VueI18n);

// move elsewhere
// VueRouter.beforeEach((to, from, next) => {
//   VuexStore.dispatch(storeRouter.CHANGE, { to, from })
//     .then((route) => {
//       if (route.forbidden || route.modified) {
//         next(route);
//       } else {
//         next();
//       }
//     });
// });
Vue.use(Router);
Vue.use(PortalVue);
Vue.use(VeeValidate);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
