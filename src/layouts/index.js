export default {
  'nav-default': () => import(/* webpackChunkName: "default" */ './l-nav-default'),
  'nav-left': () => import(/* webpackChunkName: "left" */ './l-nav-left'),
  'nav-right': () => import(/* webpackChunkName: "right" */ './l-nav-right'),
};
