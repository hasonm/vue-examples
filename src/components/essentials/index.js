// import expressions from '@/components/essentials/e-expressions';
// import binding from '@/components/essentials/e-binding-class-style';

export default {
  // expressions,
  // binding,
  'binding-class-style': () => import('@/components/essentials/e-binding-class-style'),
  events: () => import('@/components/essentials/e-events'),
  expressions: () => import('@/components/essentials/e-expressions'),
  'conditional-rendering': () => import('@/components/essentials/e-conditional-rendering'),
  'list-rendering': () => import('@/components/essentials/e-list-rendering'),
  slots: () => import('@/components/essentials/e-slots'),
  'model-sync': () => import('@/components/essentials/e-v-model-sync'),
  'lifecycle-hooks': () => import('@/components/essentials/e-lifecycle-hooks'),

};
