import Vue from 'vue';

Vue.directive('cd_focus', {
  inserted(el) {
    el.focus();
  },
});


Vue.directive('cd_background', {
  bind(el, binding, vnode) {
    // console.log(el, binding);
    const color = binding.value || 'orange';
    if (el) {
      el.style.backgroundColor = color;
    }
  },
});

// bind - This occurs once the directive is attached to the element.
// inserted - This hook occurs once the element is inserted into the parent DOM.
// update - This hook is called when the element updates, but children haven't been updated yet.
//  componentUpdated - This hook is called once the component and the children have been updated.
// unbind - This hook is called once the directive is removed.
